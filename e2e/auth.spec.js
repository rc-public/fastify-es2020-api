import supertest from 'supertest'
import { createApp } from '../src/app'

describe('[E2E] Test auth controller', () => {
  let app
  let server

  const url = (path) => `/auth${path}`

  beforeAll(async () => {
    app = await createApp({ logger: undefined })
    await app.ready()
    server = supertest(app.server)
  })

  afterAll(async () => {
    await app.close()
  })

  it('should be defined', async () => {
    expect.assertions(2)

    expect(app).toBeDefined()
    expect(app.server).toBeDefined()
  })

  it('should return 200 ok with token on POST /login', async () => {
    expect.assertions(3)

    const response = await server.post(url('/login')).send({ username: 'teste', password: 'p123' })

    expect(response.body.token).toBeDefined()
    expect(response.body.user).toStrictEqual({
      username: 'teste',
    })
    expect(response.statusCode).toBe(200)
  })

  it('should return 400 bad request on POST /login when no password is provided', async () => {
    expect.assertions(1)

    const response = await server.post(url('/login')).send({ username: 'teste' })

    expect(response.statusCode).toBe(400)
  })

  it('should return 400 bad request on POST /login when no username is provided', async () => {
    expect.assertions(1)

    const response = await server.post(url('/login')).send({ password: 'p123' })

    expect(response.statusCode).toBe(400)
  })
})
