module.exports = {
  root: false,
  env: {
    'jest/globals': true,
  },
  plugins: ['jest', 'jest-formatting'],
  extends: ['plugin:jest/all', 'plugin:jest-formatting/strict'],
  rules: {
    'jest/no-hooks': 'off',
  },
}
