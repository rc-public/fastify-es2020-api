import supertest from 'supertest'
import { createApp } from '../src/app'

describe('[E2E] Test home controller', () => {
  let app
  let server

  beforeAll(async () => {
    app = await createApp({ logger: undefined })
    await app.ready()
    server = supertest(app.server)
  })

  afterAll(async () => {
    await app.close()
  })

  it('should be defined', async () => {
    expect.assertions(2)

    expect(app).toBeDefined()
    expect(app.server).toBeDefined()
  })

  it('should return 200 ok on /', async () => {
    expect.assertions(1)

    const response = await server.get('/')

    expect(response.statusCode).toBe(200)
  })

  it('should return 200 and status ok on /health', async () => {
    expect.assertions(2)

    const response = await server.get('/health')

    expect(response.statusCode).toBe(200)
    expect(response.body).toStrictEqual({ status: 'ok' })
  })

  it('should return 401 on /secret when no authentication header is provided', async () => {
    expect.assertions(1)

    const response = await server.get('/secret')

    expect(response.statusCode).toBe(401)
  })

  it('should return 200 on /secret', async () => {
    expect.assertions(1)

    const token =
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6IlJlbmF0byIsImlhdCI6MTU5MjUxOTc1NX0.YO-6PBMd03_7yj6pVWWkBsFfsfp0aDNUtocO2QaxNbU'

    const response = await server.get('/secret').set({ Authorization: `Bearer ${token}` })

    expect(response.statusCode).toBe(200)
  })
})
