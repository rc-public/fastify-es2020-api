import { createApp } from '../src/app'
import isDocker from 'is-docker'

const main = async () => {
  const app = await createApp()
  await app.ready()

  const port = process.env.PORT || 3001

  try {
    if (isDocker() || process.env.IS_HEROKU === 'true') {
      await app.listen(port, '0.0.0.0')
    } else {
      await app.listen(port)
    }
  } catch (error) {
    app.log.error(error)
    // eslint-disable-next-line unicorn/no-process-exit
    process.exit(1)
  }
}

main()
