import Fastify from 'fastify'
import helmet from 'fastify-helmet'
import compress from 'fastify-compress'
import underPressure from 'under-pressure'
import fastifyJwt from 'fastify-jwt'

import registerRoutes from './www/register-routes'

const defaults = {
  logger: true,
}

const createApp = async (options) => {
  const app = Fastify({ ...defaults, ...options })

  app.register(fastifyJwt, {
    secret: process.env.JWT_SECRET || 'E0Q5A3dCL2 pKGWz5b5Yi YMA3JtsJRp TAPnymTpuW',
  })

  app.decorate('authenticate', async (request, reply) => {
    try {
      await request.jwtVerify()
    } catch (error) {
      reply.send(error)
    }
  })

  await registerRoutes(app)

  app.register(underPressure, {
    exposeStatusRoute: {
      routeOpts: {
        logLevel: 'debug',
      },
      url: '/health',
    },
  })

  app.register(helmet)
  app.register(compress, { global: false })

  return app
}

export { createApp }
