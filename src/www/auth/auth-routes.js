import { loginSchema } from './auth-schemas'

const authRoutes = (fastify, opts, done) => {
  fastify.post('/login', { schema: loginSchema }, (request, reply) => {
    const { password, ...user } = request.body

    const token = fastify.jwt.sign(user)

    reply.send({ user, token })
  })

  done()
}

export default authRoutes
