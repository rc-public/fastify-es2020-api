import S from 'fluent-schema'

const loginBodySchema = S.object().prop('username', S.string().required()).prop('password', S.string().required())

const loginResponseSchema = {
  '2xx': {
    type: 'object',
    properties: {
      user: {
        type: 'object',
        properties: {
          username: { type: 'string' },
        },
      },
      token: { type: 'string' },
    },
  },
}

const loginSchema = {
  body: loginBodySchema,
  response: loginResponseSchema,
}

export { loginSchema }
