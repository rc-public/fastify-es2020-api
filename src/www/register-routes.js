import getRoutes from './get-routes'

const addRoutes = async (app, route) => {
  // eslint-disable-next-line extra-rules/no-commented-out-code
  const module = await import(/* webpackChunkName: "[request]" */ `./${route}/${route}-routes`)
  const prefix = route === 'home' ? undefined : `/${route}`

  app.register(module.default, { prefix })
  app.log.info({ prefix: route }, 'routes added')
}

const registerRoutes = async (app) => {
  const routesToRegister = await getRoutes()
  const addingRoutesPromises = routesToRegister.map((route) => addRoutes(app, route))

  await Promise.all(addingRoutesPromises)
}

export default registerRoutes
