const homeRoutes = (fastify, opts, done) => {
  const welcome = 'Fastify ES2020 API'

  fastify.get('/', (request, reply) => {
    reply.send(welcome)
  })

  fastify.get(
    '/secret',
    {
      preValidation: [fastify.authenticate],
    },
    (request, reply) => {
      const { username } = request.user

      reply.send(`${welcome} ${username}!`)
    }
  )

  done()
}

export default homeRoutes
