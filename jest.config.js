module.exports = {
  transform: {
    '^.+\\.jsx?$': '@sucrase/jest-plugin',
  },
  testEnvironment: 'node',
  collectCoverageFrom: ['src/**/*.{js,ts,jsx}', '!src/main.js', '!src/www/get-routes.js'],
  coverageDirectory: 'coverage',
  coverageThreshold: {
    global: {
      branches: 85,
      functions: 90,
      lines: 90,
      statements: 90,
    },
  },
}
