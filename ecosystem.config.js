// Options reference: https://pm2.io/doc/en/runtime/reference/ecosystem-file/
module.exports = {
  apps: [
    {
      exec_mode: 'cluster',
      instances: 'max',
      name: 'main',
      script: 'dist/main.js',
      watch: false,
    },
  ],
}
