const webpack = require('webpack')
const nodeExternals = require('webpack-node-externals')
const CopyWebpackPlugin = require('copy-webpack-plugin')

const { build } = require('../scripts/options')

module.exports = {
  target: 'node',
  mode: 'production',
  devtool: 'none',
  entry: [].concat(build.entry),
  externals: [nodeExternals()],
  output: {
    filename: '[name].js',
    chunkFilename: '[hash:6].js',
    libraryTarget: 'commonjs2',
    path: build.output,
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              configFile: build.babel,
            },
          },
        ],
      },
    ],
  },
  resolve: build.resolve,
  node: build.node,
  plugins: [
    // eslint-disable-next-line unicorn/explicit-length-check
    ...build.copy && build.copy.length ? [new CopyWebpackPlugin({ patterns: build.copy })] : [],
    new webpack.DefinePlugin(build.globals),
    new webpack.NormalModuleReplacementPlugin(/^es6-promisify$/, 'util'),
    new webpack.NormalModuleReplacementPlugin(/^node-uuid$/, 'uuid'),
    new webpack.HashedModuleIdsPlugin(),
    new webpack.optimize.LimitChunkCountPlugin({
      maxChunks: 1,
    }),
  ],
}
