FROM node:alpine

RUN mkdir /home/node/app
WORKDIR /home/node/app

COPY ./ ./

RUN yarn global add pm2@4.4.0

RUN yarn install --production --pure-lockfile --non-interactive --cache-folder ./yarncache; rm -rf ./yarncache

RUN yarn run build

EXPOSE 3000
CMD ["pm2-runtime", "ecosystem.config.js", "--only", "main"]
