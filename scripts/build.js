const debug = require('debug')
const rimraf = require('rimraf')
const webpack = require('webpack')

const log = {
  error: debug('scripts:build:error'),
  info: debug('scripts:build'),
}

const options = require('./options')
// eslint-disable-next-line security/detect-non-literal-require
const webpackConfig = require(options.webpack.config)

log.info('START')

log.info('cleaning output folder')
rimraf.sync(options.build.output)

log.info('compiling....')
const compiler = webpack(webpackConfig)

compiler.run((err, stats) => {
  if (err) log.error(err)
  else log.info(stats.toString(options.webpack.stats))
  log.info('DONE')
})
