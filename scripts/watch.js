const debug = require('debug')
const webpack = require('webpack')
const rimraf = require('rimraf')
const { replace } = require('lodash')

const options = require('./options')
// eslint-disable-next-line security/detect-non-literal-require
const webpackConfig = require(options.webpack.config)

const log = {
  error: debug('scripts:watch:error'),
  info: debug('scripts:watch'),
}

rimraf.sync(options.build.output)

const compiler = webpack(webpackConfig)
compiler.watch(
  {
    ignored: /node_modules|dist|logs/,
  },
  (err, stats) => {
    if (err) log.error(err)
    else {
      const result = stats.toString(options.webpack.stats)
      if (options.env !== 'development') console.info(result)
      else log.info(replace(result, '\n', ' | '))
    }
  }
)
